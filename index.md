# deanturpin/gcc

See the complete documentation on [Docker](https://hub.docker.com/r/deanturpin/gcc) and the output from the [last build](https://deanturpin.gitlab.io/gcc/).

```bash
docker run deanturpin/gcc
```

## Building locally

To build the Docker image clone this repo and run:

```bash
docker build --no-cache -t deanturpin/gcc .
```

## Building of Apple silicon

```bash
docker buildx build --no-cache --platform linux/arm64/v8 -t deanturpin/gcc-arm64 .
```

