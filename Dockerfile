# Start with the latest OS and get it up-to-date
FROM ubuntu:devel AS build
RUN apt update && \
	apt full-upgrade --yes && \
	apt install --yes git make

# Shallow clone the source
RUN git clone --depth=1 git://gcc.gnu.org/git/gcc.git

# Configure the compiler
RUN apt install --yes libgmp3-dev libmpfr-dev libmpc-dev libz-dev flex file clang
RUN mkdir build
WORKDIR build
RUN ../gcc/configure --enable-languages=c++ --disable-multilib --with-system-zlib --disable-bootstrap

# Build and install
RUN make --silent -j $(nproc)
RUN make -j $(nproc) install

# Start next stage and copy the artifacts
FROM ubuntu:devel

# Essential packages for dev
ENV PACKAGES "vim git curl file htop parallel tree tmux make duf" \
	"cmake ninja-build vim time neofetch figlet mold entr" \
	"libgtest-dev libbenchmark-dev libtbb-dev libpcap-dev" \
	"python3 python3-pip python3-all-venv" \
	"ubuntu-release-upgrader-core pandoc sloccount" \
	"iputils-ping nmap gdb"

RUN apt update && \
	apt full-upgrade --yes && \
	apt install --yes $PACKAGES

# Copy build artifacts from first stage
COPY --from=build /usr/local /usr/local

# Dump some version info
WORKDIR /run
CMD figlet -f small -w 60 docker run deanturpin/gcc && \
	neofetch --stdout && \
	g++ --version && \
	xz --version && \
	dpkg -l $PACKAGES
